/*
 * ADC.c
 *
 *  Created on: 02.02.2016
 *      Author: Michael Lipp
 */

#include "dashboard.h"

uint16_t ADC_get_value(uint8_t Addr, uint8_t Channel)
{
	uint16_t 	ADC_value = 0;
	uint8_t		high,low = 0;									// high und Low auslese Variable f�r uniq

	if (i2c_start(Addr + I2C_WRITE)==0)							// Safe_System Abfrage ob Maxim erreichbar, liefert wenn erreichbar Wert 0 zur�ck
	{
		i2c_start_wait(Addr + I2C_WRITE);						// Rufe jeweiligen ADC �ber Adresse auf
		i2c_write(Channel);										// Rufe gew�nschten Channel des ADC auf
		i2c_stop();												// Bus wieder freigeben

		i2c_rep_start(Addr + I2C_READ);							// Rufe wieder jeweiligen ADC auf, aber mit Leseanfrage
		low		= i2c_read(1);									// Lese 'low'-Wert ein
		high 	= i2c_read(0);									// Lese 'high'-Wert ein
		i2c_stop();												// Bus wieder freigeben

		ADC_value = ((((low << 8) | high) >> 4 ) & 0x0FFF);		// Beschreibe die Variable mit dem eben gemessenen Wert
	}

	return(ADC_value);
}

