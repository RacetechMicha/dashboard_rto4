/*
 * ADC.h
 *
 *  Created on: 31.01.2016
 *      Author: Michael
 */

#ifndef ADC_H_
#define ADC_H_

#include "dashboard.h"

//Adress-definitions for all ADCs
#define ADC000			0b01010000			//Adresse des ersten Drehschalter - ADCs 	MAX127
#define ADC100			0b01010010			//Adresse des zweiten Drehschalter - ADCs	MAX127
#define ADC110			0b01010110			//Adresse des dritten Drehschalter - ADCs 	MAX127
#define ADC111			0b01011110			//Adresse des vierten ADCs 					MAX127

//Input-definitions for ADC000
#define S1_2			0b10000000			//Schalter 1, Stellung 2, CH0, 0-Vref, no Power-Down, normal operation
#define S1_3			0b10010000			//Schalter 1, Stellung 3, CH1, 0-Vref, no Power-Down, normal operation
#define S1_4			0b10100000			//Schalter 1, Stellung 4, CH2, 0-Vref, no Power-Down, normal operation
#define S1_5			0b10110000			//Schalter 1, Stellung 5, CH3, 0-Vref, no Power-Down, normal operation
#define S1_6			0b11000000			//Schalter 1, Stellung 6, CH4, 0-Vref, no Power-Down, normal operation
#define S1_7			0b11010000			//Schalter 1, Stellung 7, CH5, 0-Vref, no Power-Down, normal operation
#define S1_8			0b11100000			//Schalter 1, Stellung 8, CH6, 0-Vref, no Power-Down, normal operation
#define S1_9			0b11110000			//Schalter 1, Stellung 9, CH7, 0-Vref, no Power-Down, normal operation

//Input-definitions for ADC100
#define S1_10			0b10000000			//Schalter 1, Stellung 10, CH0, 0-Vref, no Power-Down, normal operation
#define S1_11			0b10010000			//Schalter 1, Stellung 11, CH1, 0-Vref, no Power-Down, normal operation
#define S1_12			0b10100000			//Schalter 1, Stellung 12, CH2, 0-Vref, no Power-Down, normal operation
#define S2_1			0b10110000			//Schalter 2, Stellung 1, CH3, 0-Vref, no Power-Down, normal operation
#define S2_2			0b11000000			//Schalter 2, Stellung 2, CH4, 0-Vref, no Power-Down, normal operation
#define S2_3			0b11010000			//Schalter 2, Stellung 3, CH5, 0-Vref, no Power-Down, normal operation
#define S2_4			0b11100000			//Schalter 2, Stellung 4, CH6, 0-Vref, no Power-Down, normal operation
#define S2_5			0b11110000			//Schalter 2, Stellung 5, CH7, 0-Vref, no Power-Down, normal operation

//Input-definitions for ADC110
#define S2_6			0b10000000			//Schalter 2, Stellung 6, CH0, 0-Vref, no Power-Down, normal operation
#define S2_7			0b10010000			//Schalter 2, Stellung 7, CH1, 0-Vref, no Power-Down, normal operation
#define S2_8			0b10100000			//Schalter 2, Stellung 8, CH2, 0-Vref, no Power-Down, normal operation
#define S2_9			0b10110000			//Schalter 2, Stellung 9, CH3, 0-Vref, no Power-Down, normal operation
#define S2_10			0b11000000			//Schalter 2, Stellung 10, CH4, 0-Vref, no Power-Down, normal operation
#define S2_11			0b11010000			//Schalter 2, Stellung 11, CH5, 0-Vref, no Power-Down, normal operation
#define S2_12			0b11100000			//Schalter 2, Stellung 12, CH6, 0-Vref, no Power-Down, normal operation
#define DrehPoti		0b11110000			//Analoger Eingang des Drehpotentiometers

//Input-definitions for ADC111
#define CH0				0b10000000			//GPInput CH0, 0-5V, no Power-Down, normal operation
#define CH1				0b10010000			//GPInput CH1, 0-5V, no Power-Down, normal operation
#define CH2				0b10100000			//GPInput CH2, 0-5V, no Power-Down, normal operation
#define CH3				0b10110000			//GPInput CH3, 0-5V, no Power-Down, normal operation
#define CH4				0b11000000			//GPInput CH4, 0-5V, no Power-Down, normal operation
#define CH5				0b11010000			//GPInput CH5, 0-5V, no Power-Down, normal operation
#define CH6				0b11100000			//GPInput CH6, 0-5V, no Power-Down, normal operation
#define CH7				0b11110000			//GPInput CH7, 0-5V, no Power-Down, normal operation

//global variable
#define maxValue_ADC	50					//ADC-Werte, die gr��er sind, werden als '1' gewertet

#define Switch1_2		ADC000, S1_2
#define Switch1_3		ADC000, S1_3
#define Switch1_4		ADC000, S1_4
#define Switch1_5		ADC000, S1_5
#define Switch1_6		ADC000, S1_6
#define Switch1_7		ADC000, S1_7
#define Switch1_8		ADC000, S1_8
#define Switch1_9		ADC000, S1_9
#define Switch1_10		ADC100, S1_10
#define Switch1_11		ADC100, S1_11
#define Switch1_12		ADC100, S1_12

#define Switch2_1		ADC100, S2_1
#define Switch2_2		ADC100, S2_2
#define Switch2_3		ADC100, S2_3
#define Switch2_4		ADC100, S2_4
#define Switch2_5		ADC100, S2_5
#define Switch2_6		ADC110, S2_6
#define Switch2_7		ADC110, S2_7
#define Switch2_8		ADC110, S2_8
#define Switch2_9		ADC110, S2_9
#define Switch2_10		ADC110, S2_10
#define Switch2_11		ADC110, S2_11
#define Switch2_12		ADC110, S2_12

#define RotSwitch		ADC110, DrehPoti

//Function - prototypes for ADC-issues

uint16_t getParameterValue (uint8_t SwitchNr);
uint16_t ADC_get_value(uint8_t Addr, uint8_t Channel);
uint8_t getSwitchStateRight(void);
uint8_t getSwitchStateLeft(void);
uint8_t ADC_getAdress(uint8_t Nr);
uint8_t ADC_getChannel(uint8_t Nr);
void defineMinMaxParameter(void);
void sounds(void);


#endif /* ADC_H_ */
