/*
 * CAN.c
 *
 *  Created on: 05.02.2016
 *      Author: Michael Lipp
 */

#include "dashboard.h"

void send_CAN_Status(void)
{
	can_t MOb_Dashboard_ID =
	{
		.idm	= 0xFFFFFFFF,
		.id		= Dashboard_ID,
		.length	= 0
	};
	CAN_enableMOB(14, RECEIVE_DATA, MOb_Dashboard_ID);
	CAN_sendData(14, MOb_Dashboard_ID);
	CAN_disableMOB(14);
}

void define_MObs(void)
{
can_t MOb_Drehzahl_vorne =						// Message_Object f�r Drehzahl vorne
{
		.idm 	= 0b11111111110,
		.id		= Drehzahl_vorne_links_ID
};

can_t MOb_Pedale =								// Message_Object f�r Pedale
{
		.idm 	= 0b11101111010,
		.id		= Breakped_ID
};

can_t MOb_Federweg_vorne =						// Message_Object f�r Federweg vorne
{
		.idm 	= 0b11111111110,
		.id		= Federweg_vorne_links_ID
};

can_t MOb_Not_Aus =								// Message_Object f�r Not-Aus
{
		.idm	= 0b11111111110,
		.id		= Not_vorn_ID
};

can_t MOb_Bat_Temp_Lap =						// Message_Object f�r Batterie, Temperatur und Laptime
{
		.idm 	= 0b11111100010,
		.id		= Batterie_ID
};

can_t MOb_Temp_Reifen =							// Message_Object f�r Reifentemperaturen
{
		.idm 	= 0b11111110000,
		.id		= Temp_Rad_VLL_ID
};

can_t MOb_Drehzahl_hinten =						// Message_Object f�r Drehzahl hinten
{
		.idm	= 0b11111111110,
		.id		= Drehzahl_hinten_links_ID
};

can_t MOb_Drehzahl_Motor =						// Message_Object f�r Drehzahl Motor
{
		.idm 	= 0b11111111111,
		.id		= Drehzahl_Motor_ID
};

can_t MOb_Leerlauf =							// Message_Object f�r Leerlauf
{
		.idm 	= 0b11111111111,
		.id		= Leerlauf_ID
};

can_t MOb_Federweg_hinten =						// Message_Object f�r Federweg hinten
{
		.idm 	= 0b11111111100,
		.id		= Federweg_hinten_links_ID
};

can_t MOb_Sensoren_Antrieb =					// Message_Object f�r Sensoren Antrieb
{
		.idm 	= 0b11111110000,
		.id		= Drossel_ID
};

can_t MOb_Temp_Oel =							// Message_Object f�r �ltemperatur
{
		.idm 	= 0b11111111111,
		.id		= Temp_Oel_ID
};

can_t MOb_Temp_Motor =							// Message_Object f�r Motortemperatur
{
		.idm 	= 0b11111111110,
		.id		= Temp_Motor_1_ID
};

can_t MOb_Fahrstufe =							// Message_Object f�r Fahrstufe
{
		.idm 	= 0b11111111111,
		.id		= Fahrstufe_ID
};


CAN_enableMOB(MOb_Nr_Drehzahl_vorne, 	RECEIVE_DATA, 	MOb_Drehzahl_vorne);
CAN_enableMOB(MOb_Nr_Pedale,			RECEIVE_DATA,	MOb_Pedale);
CAN_enableMOB(MOb_Nr_Federweg_vorne, 	RECEIVE_DATA, 	MOb_Federweg_vorne);
CAN_enableMOB(MOb_Nr_Not_Aus,			RECEIVE_DATA,	MOb_Not_Aus);
CAN_enableMOB(MOb_Nr_Bat_Temp_Lap, 		RECEIVE_DATA, 	MOb_Bat_Temp_Lap);
CAN_enableMOB(MOb_Nr_Temp_Reifen,		RECEIVE_DATA, 	MOb_Temp_Reifen);
CAN_enableMOB(MOb_Nr_Drehzahl_hinten,	RECEIVE_DATA,	MOb_Drehzahl_hinten);
CAN_enableMOB(MOb_Nr_Drehzahl_Motor, 	RECEIVE_DATA, 	MOb_Drehzahl_Motor);
CAN_enableMOB(MOb_Nr_Leerlauf, 			RECEIVE_DATA, 	MOb_Leerlauf);
CAN_enableMOB(MOb_Nr_Federweg_hinten, 	RECEIVE_DATA, 	MOb_Federweg_hinten);
CAN_enableMOB(MOb_Nr_Sensoren_Antrieb, 	RECEIVE_DATA, 	MOb_Sensoren_Antrieb);
CAN_enableMOB(MOb_Nr_Temp_Oel, 			RECEIVE_DATA, 	MOb_Temp_Oel);
CAN_enableMOB(MOb_Nr_Temp_Motor, 		RECEIVE_DATA, 	MOb_Temp_Motor);
CAN_enableMOB(MOb_Nr_Fahrstufe, 		RECEIVE_DATA, 	MOb_Fahrstufe);
}

void receive_CAN_interrupt(can_t msg)
{
	switch(msg.id)
	{
		case(Drehzahl_vorne_links_ID):
				Drehzahl_vorne.data[0]	= msg.data[0];
				Drehzahl_vorne.data[1]	= msg.data[1];
			break;
		case(Drehzahl_vorne_rechts_ID):
				Drehzahl_vorne.data[2]	= msg.data[0];
				Drehzahl_vorne.data[3]	= msg.data[1];
			break;
		case(Breakped_ID):
			break;
		case(Kupplung_ID):
			break;
		case(Federweg_vorne_links_ID):
			break;
		case(Federweg_vorne_rechts_ID):
			break;
		case(Not_vorn_ID):
			break;
		case(Not_Dash_ID):
			break;
		case(Batterie_ID):
				Umgebung.data[0]	= msg.data[0];
				Umgebung.data[1]	= msg.data[1];
			break;
		case(Temp_Umgebung_ID):
			break;
		case(Laptime_ID):
			break;
		case(Temp_Rad_VLL_ID):
			break;
		case(Temp_Rad_VLM_ID):
			break;
		case(Temp_Rad_VLR_ID):
			break;
		case(Temp_Rad_VRL_ID):
			break;
		case(Temp_Rad_VRM_ID):
			break;
		case(Temp_Rad_VRR_ID):
			break;
		case(Drehzahl_hinten_links_ID):
			break;
		case(Drehzahl_hinten_rechts_ID):
			break;
		case(Drehzahl_Motor_ID):
				Drehzahl_Motor.data[0]	= msg.data[0];
				Drehzahl_Motor.data[1]	= msg.data[1];
			break;
		case(Leerlauf_ID):
			break;
		case(Federweg_hinten_links_ID):
			break;
		case(Federweg_hinten_rechts_ID):
			break;
		case(Drossel_ID):
			break;
		case(Lambda_ID):
			break;
		case(Benzin_p_ID):
			break;
		case(Oel_p_ID):
				Sensor.data[4]	= msg.data[0];
				Sensor.data[5]	= msg.data[1];
			break;
		case(Tank_ID):
				Sensor.data[6] 	= msg.data[0];
				Sensor.data[7]	= msg.data[1];
			break;
		case(Temp_Oel_ID):
				Temperatur.data[0]	= msg.data[0];
				Temperatur.data[1]	= msg.data[1];
			break;
		case(Temp_Motor_1_ID):
				Temperatur.data[2]	= msg.data[0];
				Temperatur.data[3]	= msg.data[1];
			break;
		case(Temp_Motor_2_ID):
				Temperatur.data[4]	= msg.data[0];
				Temperatur.data[5]	= msg.data[1];
			break;
		case(Fahrstufe_ID):
				Fahrstufe.data[0]	= msg.data[0];
				Fahrstufe.data[1]	= msg.data[1];
			break;

		default:
			break;
	}
}

SIGNAL(SIG_CAN_INTERRUPT1){
	uint8_t		save_canpage;
 	static		can_t message;


	// Aktuelle CANPAGE sichern
 	save_canpage	= CANPAGE;

    // Index des MOB ermitteln, der den Interrupt ausgel�st hat
	uint8_t mob 	= CAN_getMOBInterrupt();

	// Falls es kein g�ltiges MOB war abbrechen
	if(mob == NOMOB){
		return;
	}

	// Objekt das den Interrupt ausgel�st hat holen
	CAN_getMOB(mob);

	// Daten des MOBs aus CANMSG auslesen
	message			= CAN_getData();

	// Id der Nachricht holen
	message.id		= CAN_getID();

	// Inhalt der Nachricht in entsprechende, Code-interne Variablen schreiben
	receive_CAN_interrupt(message);

	// RXOK-Flag l�schen
	clearbit(CANSTMOB, RXOK);

	// MOB auf Empfang und CAN 2.0B Standard setzen
	CAN_setMode(RECEIVE_DATA);

    // CANPAGE wiederherstellen
	CANPAGE		= save_canpage;
}


