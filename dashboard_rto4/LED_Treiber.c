/*
 * LED_Treiber.c
 *
 *  Created on: 02.02.2016
 *      Author: Michael
 */

#include "dashboard.h"
//#include "LED_Treiber.h"

//Counter - Variables
int counter_toggle_LED	= 0;							//Counter f�r LED - Toggle - Funktionen
int counter_run_LED		= 0;							//Counter f�r LED - Run - Funktionen
uint8_t flash = 0;

extern volatile int16_t timer_run_LED;
extern volatile int16_t timer_flash_LED_red;

void LED_Treiber (double ADDR, double CMD, double DATA)
{
	if(	i2c_start(ADDR+I2C_WRITE)== 0)  				// Safe_System Abfrage ob Maxim erreichbar, liefert wenn erreichbar Wert 0 zur�ck
		{
			i2c_start(ADDR+I2C_WRITE); 					// set device address and write mode
			i2c_write(CMD);         		   			// write command
			i2c_write(DATA);                			// write data
			i2c_stop();                     			// set stop conditon = release bus
		}
}

//Initialisierung des LED-Treibers per I�C

void initialize_LED_Treiber(void)
{
//	sbi(PORTB,4);										//LEDs anschalten

	for(int i=0x09;i<0x10;i++)
	{
		LED_Treiber(Addr_LED_Drv,i,0x00);				//Alle Ports als Stromsenke definieren
	}

	for(int i=0x12;i<0x20;i++)
	{
		LED_Treiber(Addr_LED_Drv,i,LED_Helligkeit);		//Str�me aller Ports auf 16/16, also 24mA einstellen
	}

	LED_Treiber(Addr_LED_Drv,0x04,0b01000001);			//Globale Einstellungen f�r den LED-Treiber: Stromsteuerung einzeln, Normaler Modus, 'Transition Detection Control' is disabled
}

void switch_LED(int LED, int Status)
{
	switch(LED)
		{
			case (LED_IMD_green):		LED_Treiber(Addr_LED_Drv,LED_IMD_green,		Status);	break;
			case (LED_BMS_green):		LED_Treiber(Addr_LED_Drv,LED_BMS_green,		Status); 	break;
			case (LED_BSPD_green):		LED_Treiber(Addr_LED_Drv,LED_BSPD_green,	Status); 	break;
			case (LED_IS_green):		LED_Treiber(Addr_LED_Drv,LED_IS_green,		Status); 	break;
			case (LED_IS_red):			LED_Treiber(Addr_LED_Drv,LED_IS_red,		Status); 	break;
			case (LED_SBD_green):		LED_Treiber(Addr_LED_Drv,LED_SBD_green,		Status); 	break;
			case (LED_SBD_red):			LED_Treiber(Addr_LED_Drv,LED_SBD_red,		Status); 	break;
			case (LED_SBl_green):		LED_Treiber(Addr_LED_Drv,LED_SBl_green,		Status); 	break;
			case (LED_SBl_red):			LED_Treiber(Addr_LED_Drv,LED_SBl_red,		Status); 	break;
			case (LED_SBr_green):		LED_Treiber(Addr_LED_Drv,LED_SBr_green,		Status); 	break;
			case (LED_SBr_red):			LED_Treiber(Addr_LED_Drv,LED_SBr_red,		Status); 	break;
			case (LED_BOTS_green):		LED_Treiber(Addr_LED_Drv,LED_BOTS_green,	Status);	break;
			case (LED_BOTS_red):		LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		Status); 	break;
			case (LED_BMS_Temp_green):	LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,Status); 	break;
			case (LED_BMS_Temp_red):	LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	Status); 	break;
			case (LED_BMS_UV_green):	LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	Status); 	break;
			case (LED_BMS_UV_red):		LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	Status);	break;
			case (LED_BMS_OV_green):	LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	Status); 	break;
			case (LED_BMS_OV_red):		LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	Status); 	break;
			case (LED_DRS_green):		LED_Treiber(Addr_LED_Drv,LED_DRS_green,		Status); 	break;
			case (LED_DRS_red):			LED_Treiber(Addr_LED_Drv,LED_DRS_red,		Status); 	break;
			case (LED_Kl50):			LED_Treiber(Addr_LED_Drv,LED_Kl50,			Status); 	break;
			case (LED_Inv_Res):			LED_Treiber(Addr_LED_Drv,LED_Inv_Res,		Status); 	break;
			case (LED_VDCU_Res):		LED_Treiber(Addr_LED_Drv,LED_VDCU_Res,		Status); 	break;
			case (LED_Ped_Cal):			LED_Treiber(Addr_LED_Drv,LED_Ped_Cal,		Status); 	break;
			case (LED_Susp_Cal):		LED_Treiber(Addr_LED_Drv,LED_Susp_Cal,		Status); 	break;
			case (LED_Reserve):			LED_Treiber(Addr_LED_Drv,LED_Reserve,		Status); 	break;

			default:																			break;
			}
}


//Macros for starting several LEDs

void switch_LED_green(int Status)
{
//	LED_Treiber(Addr_LED_Drv,LED_IMD_green,		Status);
//	LED_Treiber(Addr_LED_Drv,LED_BMS_green,		Status);
//	LED_Treiber(Addr_LED_Drv,LED_BSPD_green,	Status);

	LED_Treiber(Addr_LED_Drv,LED_IS_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBD_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBl_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBr_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_BOTS_green,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	Status);
	LED_Treiber(Addr_LED_Drv,LED_DRS_green,		Status);
}

void switch_LED_red(int Status)
{
	LED_Treiber(Addr_LED_Drv,LED_IS_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBD_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBl_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBr_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	Status);
	LED_Treiber(Addr_LED_Drv,LED_DRS_red,		Status);
}

void switch_LED_all(int Status)
{
//	LED_Treiber(Addr_LED_Drv,LED_IMD_green,		Status);
//	LED_Treiber(Addr_LED_Drv,LED_BMS_green,		Status);
//	LED_Treiber(Addr_LED_Drv,LED_BSPD_green,	Status);

	LED_Treiber(Addr_LED_Drv,LED_IS_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_IS_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBD_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBD_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBl_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBl_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBr_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_SBr_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_BOTS_green,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	Status);
	LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	Status);
	LED_Treiber(Addr_LED_Drv,LED_DRS_green,		Status);
	LED_Treiber(Addr_LED_Drv,LED_DRS_red,		Status);
}

void run_LED_red_1(void)
{
	int Status = LED_ON;

	if(counter_run_LED == 0)
	{
		switch_LED_all(LED_OFF);
		counter_run_LED ++;
	}

	else if(counter_run_LED == 1)
		{
			LED_Treiber(Addr_LED_Drv,LED_IS_red,		Status);
			counter_run_LED ++;
		}

	else if(counter_run_LED == 2)
		{
			LED_Treiber(Addr_LED_Drv,LED_SBD_red,		Status);
			counter_run_LED ++;
		}
	else if(counter_run_LED == 3)
		{
			LED_Treiber(Addr_LED_Drv,LED_SBl_red,		Status);
			counter_run_LED ++;
		}
	else if(counter_run_LED == 4)
		{
			LED_Treiber(Addr_LED_Drv,LED_SBr_red,		Status);
			counter_run_LED ++;
		}
	else if(counter_run_LED == 5)
		{
			LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		Status);
			counter_run_LED ++;
		}
	else if(counter_run_LED == 6)
		{
			LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	Status);
			counter_run_LED ++;
		}
	else if(counter_run_LED == 7)
		{
			LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	Status);
			counter_run_LED ++;
		}
	else if(counter_run_LED == 8)
		{
			LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	Status);
			counter_run_LED ++;
		}
	else if(counter_run_LED == 9)
		{
			LED_Treiber(Addr_LED_Drv,LED_DRS_red,		Status);
			counter_run_LED ++;
		}

	Status = LED_OFF;

	if(counter_run_LED == 10)
		{
			LED_Treiber(Addr_LED_Drv,LED_IS_red,		Status);
			counter_run_LED ++;
		}

		else if(counter_run_LED == 11)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBD_red,		Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 12)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBl_red,		Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 13)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBr_red,		Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 14)
			{
				LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 15)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 16)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 17)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 18)
			{
				LED_Treiber(Addr_LED_Drv,LED_DRS_red,		Status);
				counter_run_LED = 0;
			}

}

void run_LED_red_2(void)
{
		if(counter_run_LED == 0)
		{
			switch_LED_all(LED_OFF);
			counter_run_LED ++;
		}

		else if(counter_run_LED == 1)
			{
				LED_Treiber(Addr_LED_Drv,LED_IS_red,		LED_ON);
				counter_run_LED ++;
			}

		else if(counter_run_LED == 2)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBD_red,		LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_IS_red,		LED_OFF);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 3)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBl_red,		LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_SBD_red,		LED_OFF);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 4)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBr_red,		LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_SBl_red,		LED_OFF);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 5)
			{
				LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_SBr_red,		LED_OFF);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 6)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		LED_OFF);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 7)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	LED_OFF);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 8)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	LED_OFF);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 9)
			{
				LED_Treiber(Addr_LED_Drv,LED_DRS_red,		LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	LED_OFF);
				counter_run_LED ++;
			}

		else if(counter_run_LED == 10)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	LED_ON);
				LED_Treiber(Addr_LED_Drv,LED_DRS_red,		LED_OFF);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 11)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_OV_red,	LED_OFF);
				LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	LED_ON);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 12)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_UV_red,	LED_OFF);
				LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	LED_ON);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 13)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_red,	LED_OFF);
				LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		LED_ON);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 14)
			{
				LED_Treiber(Addr_LED_Drv,LED_BOTS_red,		LED_OFF);
				LED_Treiber(Addr_LED_Drv,LED_SBr_red,		LED_ON);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 15)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBr_red,		LED_OFF);
				LED_Treiber(Addr_LED_Drv,LED_SBl_red,		LED_ON);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 16)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBl_red,		LED_OFF);
				LED_Treiber(Addr_LED_Drv,LED_SBD_red,		LED_ON);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 17)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBD_red,		LED_OFF);
				LED_Treiber(Addr_LED_Drv,LED_IS_red,		LED_ON);
				counter_run_LED = 2;
			}

}

void run_LED_red_3(void)
{

}

void run_LED_red_4(void)
{

}

void run_LED_green_1(void)
{
	int Status = LED_ON;

		if(counter_run_LED == 0)
		{
			switch_LED_all(LED_OFF);
			counter_run_LED ++;
		}

		else if(counter_run_LED == 1)
			{
				LED_Treiber(Addr_LED_Drv,LED_IS_green,		Status);
				counter_run_LED ++;
			}

		else if(counter_run_LED == 2)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBD_green,		Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 3)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBl_green,		Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 4)
			{
				LED_Treiber(Addr_LED_Drv,LED_SBr_green,		Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 5)
			{
				LED_Treiber(Addr_LED_Drv,LED_BOTS_green,		Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 6)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,	Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 7)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 8)
			{
				LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	Status);
				counter_run_LED ++;
			}
		else if(counter_run_LED == 9)
			{
				LED_Treiber(Addr_LED_Drv,LED_DRS_green,		Status);
				counter_run_LED ++;
			}

		Status = LED_OFF;

		if(counter_run_LED == 10)
			{
				LED_Treiber(Addr_LED_Drv,LED_IS_green,		Status);
				counter_run_LED ++;
			}

			else if(counter_run_LED == 11)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBD_green,		Status);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 12)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBl_green,		Status);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 13)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBr_green,		Status);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 14)
				{
					LED_Treiber(Addr_LED_Drv,LED_BOTS_green,		Status);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 15)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,	Status);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 16)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	Status);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 17)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	Status);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 18)
				{
					LED_Treiber(Addr_LED_Drv,LED_DRS_green,		Status);
					counter_run_LED = 0;
				}

}

void run_LED_green_2(void)
{
	if(counter_run_LED == 0)
			{
				switch_LED_all(LED_OFF);
				counter_run_LED ++;
			}

			else if(counter_run_LED == 1)
				{
					LED_Treiber(Addr_LED_Drv,LED_IS_green,		LED_ON);
					counter_run_LED ++;
				}

			else if(counter_run_LED == 2)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBD_green,		LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_IS_green,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 3)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBl_green,		LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_SBD_green,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 4)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBr_green,		LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_SBl_green,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 5)
				{
					LED_Treiber(Addr_LED_Drv,LED_BOTS_green,	LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_SBr_green,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 6)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_BOTS_green,	LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 7)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 8)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 9)
				{
					LED_Treiber(Addr_LED_Drv,LED_DRS_green,		LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	LED_OFF);
					counter_run_LED ++;
				}

			else if(counter_run_LED == 10)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_DRS_green,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 11)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_OV_green,	LED_OFF);
					LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	LED_ON);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 12)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_UV_green,	LED_OFF);
					LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,LED_ON);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 13)
				{
					LED_Treiber(Addr_LED_Drv,LED_BMS_Temp_green,LED_OFF);
					LED_Treiber(Addr_LED_Drv,LED_BOTS_green,	LED_ON);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 14)
				{
					LED_Treiber(Addr_LED_Drv,LED_BOTS_green,	LED_OFF);
					LED_Treiber(Addr_LED_Drv,LED_SBr_green,		LED_ON);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 15)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBr_green,		LED_OFF);
					LED_Treiber(Addr_LED_Drv,LED_SBl_green,		LED_ON);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 16)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBl_green,		LED_OFF);
					LED_Treiber(Addr_LED_Drv,LED_SBD_green,		LED_ON);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 17)
				{
					LED_Treiber(Addr_LED_Drv,LED_SBD_green,		LED_OFF);
					LED_Treiber(Addr_LED_Drv,LED_IS_green,		LED_ON);
					counter_run_LED = 2;
				}

}

void run_LED_green_3(void)
{

}

void run_LED_green_4(void)
{

}

void run_LED_all_1(void)
{

}

void run_LED_all_2(void)
{

}

void run_LED_all_3(void)
{
	if(timer_run_LED <= 150)
	{
		switch_LED_green(LED_ON);
		switch_LED_red(LED_OFF);
		if(timer_run_LED <= 0)
		{
			timer_run_LED = 450;
		}
	}
	if(timer_run_LED > 150 && timer_run_LED < 300)
	{
		switch_LED_green(LED_OFF);
		switch_LED_red(LED_ON);
	}

	if(timer_run_LED > 300)
	{
		switch_LED_all(LED_ON);
	}
}

void run_LED_all_4(void)
{

	if(counter_run_LED == 0)
			{
				switch_LED(LED_Kl50, LED_OFF);
				switch_LED(LED_Inv_Res, LED_OFF);
				switch_LED(LED_VDCU_Res, LED_OFF);
				counter_run_LED ++;
			}

			else if(counter_run_LED == 1)
				{
					LED_Treiber(Addr_LED_Drv,LED_Kl50,			LED_ON);
					counter_run_LED ++;
				}

			else if(counter_run_LED == 2)
				{
					LED_Treiber(Addr_LED_Drv,LED_Inv_Res,		LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_Kl50,			LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 3)
				{
					LED_Treiber(Addr_LED_Drv,LED_VDCU_Res,		LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_Inv_Res,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 4)
				{
					LED_Treiber(Addr_LED_Drv,LED_VDCU_Res,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 5)
				{
					LED_Treiber(Addr_LED_Drv,LED_VDCU_Res,		LED_ON);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 6)
				{
					LED_Treiber(Addr_LED_Drv,LED_Inv_Res,		LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_VDCU_Res,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 7)
				{
					LED_Treiber(Addr_LED_Drv,LED_Kl50,			LED_ON);
					LED_Treiber(Addr_LED_Drv,LED_Inv_Res,		LED_OFF);
					counter_run_LED ++;
				}
			else if(counter_run_LED == 8)
				{
					LED_Treiber(Addr_LED_Drv,LED_Kl50,			LED_OFF);
					counter_run_LED = 1;
				}


}

void toggle_LED_1(void)
{
	if(counter_toggle_LED == 0)
	{
		switch_LED_green(LED_ON);
		counter_toggle_LED ++;
	}
	else if(counter_toggle_LED == 1)
	{
		switch_LED_green(LED_OFF);
		counter_toggle_LED = 0;
	}

}

void toggle_LED_2(void)
{
	if(counter_toggle_LED == 0)
	{
		switch_LED_red(LED_ON);
		counter_toggle_LED ++;
	}
	else if(counter_toggle_LED == 1)
	{
		switch_LED_red(LED_OFF);
		counter_toggle_LED = 0;
	}

}

void toggle_LED_3(void)
{
	if(counter_toggle_LED == 0)
	{
		switch_LED_red(LED_ON);
		switch_LED_green(LED_ON);
		counter_toggle_LED ++;
	}
	else if(counter_toggle_LED == 1)
	{
		switch_LED_red(LED_OFF);
		switch_LED_green(LED_OFF);
		counter_toggle_LED = 0;
	}

}

void toggle_LED_4(void)
{
		if(counter_toggle_LED == 0)
		{
			switch_LED_green(LED_ON);
			counter_toggle_LED ++;
		}
		else if(counter_toggle_LED == 1)
		{
			switch_LED_red(LED_ON);
			counter_toggle_LED ++;
		}
		else if(counter_toggle_LED == 2)
		{
			switch_LED_green(LED_OFF);
			counter_toggle_LED ++;
		}
		else if(counter_toggle_LED == 3)
		{
			switch_LED_red(LED_OFF);
			counter_toggle_LED = 0;
		}
}


void DrehzahlMotor(void)
{
	flash++;				//Z�hlvariable f�r Drehzahl >11000 RPM

	if(Drehzahl_Motor.translation.links < 2000)
	{
		switch_LED(LED_IS_green, LED_OFF);
		switch_LED(LED_SBD_green, LED_OFF);
		switch_LED(LED_SBl_green, LED_OFF);
		switch_LED(LED_SBr_green, LED_OFF);
		switch_LED(LED_BOTS_green, LED_OFF);
		switch_LED(LED_BMS_Temp_green, LED_OFF);
		switch_LED(LED_BMS_UV_green, LED_OFF);
		switch_LED(LED_BMS_OV_green, LED_OFF);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 3000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_OFF);
		switch_LED(LED_SBl_green, LED_OFF);
		switch_LED(LED_SBr_green, LED_OFF);
		switch_LED(LED_BOTS_green, LED_OFF);
		switch_LED(LED_BMS_Temp_green, LED_OFF);
		switch_LED(LED_BMS_UV_green, LED_OFF);
		switch_LED(LED_BMS_OV_green, LED_OFF);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 4000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_ON);
		switch_LED(LED_SBl_green, LED_OFF);
		switch_LED(LED_SBr_green, LED_OFF);
		switch_LED(LED_BOTS_green, LED_OFF);
		switch_LED(LED_BMS_Temp_green, LED_OFF);
		switch_LED(LED_BMS_UV_green, LED_OFF);
		switch_LED(LED_BMS_OV_green, LED_OFF);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 5000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_ON);
		switch_LED(LED_SBl_green, LED_ON);
		switch_LED(LED_SBr_green, LED_OFF);
		switch_LED(LED_BOTS_green, LED_OFF);
		switch_LED(LED_BMS_Temp_green, LED_OFF);
		switch_LED(LED_BMS_UV_green, LED_OFF);
		switch_LED(LED_BMS_OV_green, LED_OFF);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 6000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_ON);
		switch_LED(LED_SBl_green, LED_ON);
		switch_LED(LED_SBr_green, LED_ON);
		switch_LED(LED_BOTS_green, LED_OFF);
		switch_LED(LED_BMS_Temp_green, LED_OFF);
		switch_LED(LED_BMS_UV_green, LED_OFF);
		switch_LED(LED_BMS_OV_green, LED_OFF);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 7000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_ON);
		switch_LED(LED_SBl_green, LED_ON);
		switch_LED(LED_SBr_green, LED_ON);
		switch_LED(LED_BOTS_green, LED_ON);
		switch_LED(LED_BMS_Temp_green, LED_OFF);
		switch_LED(LED_BMS_UV_green, LED_OFF);
		switch_LED(LED_BMS_OV_green, LED_OFF);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 8000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_ON);
		switch_LED(LED_SBl_green, LED_ON);
		switch_LED(LED_SBr_green, LED_ON);
		switch_LED(LED_BOTS_green, LED_ON);
		switch_LED(LED_BMS_Temp_green, LED_ON);
		switch_LED(LED_BMS_UV_green, LED_OFF);
		switch_LED(LED_BMS_OV_green, LED_OFF);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 9000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_ON);
		switch_LED(LED_SBl_green, LED_ON);
		switch_LED(LED_SBr_green, LED_ON);
		switch_LED(LED_BOTS_green, LED_ON);
		switch_LED(LED_BMS_Temp_green, LED_ON);
		switch_LED(LED_BMS_UV_green, LED_ON);
		switch_LED(LED_BMS_OV_green, LED_OFF);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 10000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_ON);
		switch_LED(LED_SBl_green, LED_ON);
		switch_LED(LED_SBr_green, LED_ON);
		switch_LED(LED_BOTS_green, LED_ON);
		switch_LED(LED_BMS_Temp_green, LED_ON);
		switch_LED(LED_BMS_UV_green, LED_ON);
		switch_LED(LED_BMS_OV_green, LED_ON);
		switch_LED(LED_DRS_green, LED_OFF);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 11000)
	{
		switch_LED(LED_IS_green, LED_ON);
		switch_LED(LED_SBD_green, LED_ON);
		switch_LED(LED_SBl_green, LED_ON);
		switch_LED(LED_SBr_green, LED_ON);
		switch_LED(LED_BOTS_green, LED_ON);
		switch_LED(LED_BMS_Temp_green, LED_ON);
		switch_LED(LED_BMS_UV_green, LED_ON);
		switch_LED(LED_BMS_OV_green, LED_ON);
		switch_LED(LED_DRS_green, LED_ON);

		switch_LED_red(LED_OFF);
	}
	else if(Drehzahl_Motor.translation.links < 12000)
	{
		switch_LED_all(LED_ON);
	}
	else
	{
		if(timer_flash_LED_red <= 0)
		{
			switch_LED_green(LED_OFF);
			switch_LED_red(LED_ON);
			timer_flash_LED_red = 200;
		}
		else if(timer_flash_LED_red > 100)
		{
			switch_LED_all(LED_OFF);
		}

	}
}
