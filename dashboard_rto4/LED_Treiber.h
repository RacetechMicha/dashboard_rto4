/*
 * LED_Treiber.h
 *
 *  Created on: 31.01.2016
 *      Author: Michael
 */

#ifndef LED_TREIBER_H_
#define LED_TREIBER_H_


//Definition of adresses

#define Addr_LED_Drv			0b10000000				//Adresse des LED-Treibers

//Definition of commands

#define LED_ON					0x01					//LED anschalten
#define LED_OFF					0x00					//LED ausschalten

#define LED_Helligkeit 			0xFF					//LED - Helligkeit standardm��ig auf Maximum eingestellt (24mA)

#define LED_IMD_green			0x24					//P4 -> IMD LED gr�n
#define LED_BMS_green			0x25					//P5 -> BMS LED gr�n
#define LED_BSPD_green			0x26					//P6 -> BSPD LED gr�n
#define LED_IS_green			0x27					//P7 -> InertiaSwitch LED gr�n
#define LED_IS_red				0x28					//P8 -> InertiaSwitch LED rot
#define LED_SBD_green			0x29					//P9 -> ShutdownButton Dashboard LED gr�n
#define LED_SBD_red				0x2A					//P10 -> ShutdownButton Dashboard LED rot
#define LED_SBl_green			0x2B					//P11 -> ShutdownButton links LED gr�n
#define LED_SBl_red				0x2C					//P12 -> ShutdownButton links LED rot
#define LED_SBr_green			0x2D					//P13 -> ShutdownButton rechts LED gr�n
#define LED_SBr_red				0x2E					//P14 -> ShutdownButton rechts LED rot
#define LED_BOTS_green			0x2F					//P15 -> BrakeOverTravelSwitch LED gr�n
#define LED_BOTS_red			0x30					//P16 -> BrakeOverTravelSwitch LED rot
#define LED_BMS_Temp_green		0x31					//P17 -> BMS Temperature Status LED gr�n
#define LED_BMS_Temp_red		0x32					//P18 -> BMS Temperature Status LED rot
#define LED_BMS_UV_green		0x33					//P19 -> BMS Under-Voltage Status LED gr�n
#define LED_BMS_UV_red			0x34					//P20 -> BMS Under-Voltage Status LED rot
#define LED_BMS_OV_green		0x35					//P21 -> BMS Over-Voltage Status LED gr�n
#define LED_BMS_OV_red			0x36					//P22 -> BMS Over-Voltage Status LED rot
#define LED_DRS_green			0x37					//P23 -> DRS Status LED gr�n
#define LED_DRS_red				0x38					//P24 -> DRS Status LED rot

#define LED_Kl50				0x3C					//P25 -> Klemme 50 LED-Ring
#define LED_Inv_Res				0x3D					//P26 -> Inverter Reset LED-Ring
#define LED_VDCU_Res			0x3E					//P27 -> VDCU Reset LED-Ring
#define LED_Ped_Cal				0x39					//P28 -> Pedalkalibrierung LED-Ring
#define LED_Susp_Cal			0x3A					//P29 -> Federwegsensorkalibrierung LED-Ring
#define LED_Reserve				0x3B					//P30 -> Reserve LED-Ring


void LED_Treiber(double ADDR, double CMD, double DATA);
void initialize_LED_Treiber(void);
void translation_current(uint8_t hex_value);
void update_LEDs(void);
void switch_LED(int LED, int Status);
void switch_LED_green(int Status);
void switch_LED_red(int Status);
void switch_LED_all(int Status);
void run_LED_red_1(void);
void run_LED_red_2(void);
void run_LED_red_3(void);
void run_LED_red_4(void);
void run_LED_green_1(void);
void run_LED_green_2(void);
void run_LED_green_3(void);
void run_LED_green_4(void);
void run_LED_all_1(void);
void run_LED_all_2(void);
void run_LED_all_3(void);
void run_LED_all_4(void);
void toggle_LED_1(void);
void toggle_LED_2(void);
void toggle_LED_3(void);
void toggle_LED_4(void);

void DrehzahlMotor(void);

enum LEDs
{
	Nr_LED_IMD_green		= 0x24,
};

#endif /* LED_TREIBER_H_ */
