/*
 * can_ID.h
 *
 *  Created on: 10.05.2015
 *      Author: Michael
 */

#ifndef CAN_ID_H_
#define CAN_ID_H_

//Ausgehende Nachrichten Dashboard

#define Dashboard_ID				300
#define Schalten_Hoch_ID			320
#define	Schalten_Runter_ID			321


//Ankommende Nachrichten Dashboard

#define Drehzahl_vorne_links_ID		120
#define Drehzahl_vorne_rechts_ID	121
#define Breakped_ID					122
#define Kupplung_ID					123
#define Federweg_vorne_links_ID		124
#define Federweg_vorne_rechts_ID	125
#define Not_vorn_ID					126
#define Not_Dash_ID					127
#define Batterie_ID					138
#define Temp_Umgebung_ID			139
#define Laptime_ID					150
#define Temp_Rad_VLL_ID				180
#define Temp_Rad_VLM_ID				181
#define Temp_Rad_VLR_ID				182
#define Temp_Rad_VRL_ID				183
#define Temp_Rad_VRM_ID				184
#define Temp_Rad_VRR_ID				185
#define Drehzahl_hinten_links_ID	220
#define Drehzahl_hinten_rechts_ID	221
#define	Drehzahl_Motor_ID			222
#define Leerlauf_ID					223
#define Federweg_hinten_links_ID	225
#define Federweg_hinten_rechts_ID	226
#define Drossel_ID					227
#define Lambda_ID					228
#define Benzin_p_ID					229
#define Oel_p_ID					230
#define Tank_ID						236
#define Temp_Oel_ID					237
#define Temp_Motor_1_ID				238
#define Temp_Motor_2_ID				239
#define Fahrstufe_ID				420

enum CAN_Message_Objects
{
	MOb_Nr_Drehzahl_vorne			= 0,
	MOb_Nr_Pedale					= 1,
	MOb_Nr_Federweg_vorne			= 2,
	MOb_Nr_Not_Aus					= 3,
	MOb_Nr_Bat_Temp_Lap 			= 4,
	MOb_Nr_Temp_Reifen		 		= 5,
	MOb_Nr_Drehzahl_hinten			= 6,
	MOb_Nr_Drehzahl_Motor			= 7,
	MOb_Nr_Leerlauf					= 8,
	MOb_Nr_Federweg_hinten			= 9,
	MOb_Nr_Sensoren_Antrieb			= 10,
	MOb_Nr_Temp_Oel					= 11,
	MOb_Nr_Temp_Motor				= 12,
	MOb_Nr_Fahrstufe				= 13,
};

//Funktionsprototypen

void send_CAN_ID (void);
void send_CAN_Status(void);
void define_MObs(void);


#endif /* CAN_ID_H_ */
