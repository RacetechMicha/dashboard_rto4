/*
 * dashboard.c
 *
 *  Created on: 07.02.2016
 *      Author: Michael Lipp
 */

#include "dashboard.h"

extern volatile int16_t timer_interrupt;

void Init_Variables(void)
{
	Drehzahl_vorne.translation.links	= 0;
	Drehzahl_vorne.translation.rechts	= 0;
	Drehzahl_hinten.translation.links	= 0;
	Drehzahl_hinten.translation.rechts	= 0;
	Drehzahl_Motor.translation.links	= 0;

	Pedal.translation.Bremse			= 0;
	Pedal.translation.Drossel			= 0;
	Pedal.translation.Kupplung			= 0;
	Pedal.translation.Leerlauf			= 0;

	Federweg_vorne.translation.links	= 0;
	Federweg_vorne.translation.rechts	= 0;
	Federweg_hinten.translation.links	= 0;
	Federweg_hinten.translation.rechts	= 0;

	Not_Aus.translation.Dashboard		= 0;
	Not_Aus.translation.vorn			= 0;

	Umgebung.translation.Batterie		= 0;
	Umgebung.translation.Laptime		= 0;
	Umgebung.translation.Temp_Umgebung	= 0;

//	Reifentemperatur_vorne.translation.links = 0;
//	Reifentemperatur_vorne.translation.mitte = 0;
//	Reifentemperatur_vorne.translation.rechts = 0;
//	Reifentemperatur_hinten.translation.links = 0;
//	Reifentemperatur_hinten.translation.mitte = 0;
//	Reifentemperatur_hinten.translation.rechts = 0;

	Sensor.translation.Benzin_p			= 0;
	Sensor.translation.Lambda			= 0;
	Sensor.translation.Oel_p			= 0;
	Sensor.translation.Tank				= 0;

	Temperatur.translation.Motor_1		= 0;
	Temperatur.translation.Motor_2		= 0;
	Temperatur.translation.Oel			= 0;

	Fahrstufe.translation.value			= 0;

}

//Interruptroutine zum Runterschalten
ISR(SIG_INTERRUPT2)
{
	if(timer_interrupt <= 0)
	{
		can_t MOb_Runterschalten =
		{
			.idm	= 0xFFFFFFFF,
			.id		= Schalten_Runter_ID,
			.length	= 1
		};
		CAN_enableMOB(14, RECEIVE_DATA, MOb_Runterschalten);
		CAN_sendData(14, MOb_Runterschalten);
		CAN_disableMOB(14);

		timer_interrupt = 200;
	}
}

//Interruptroutine zum Hochschalten
ISR(SIG_INTERRUPT3)
{
	if(timer_interrupt <= 0)
	{
		can_t MOb_Hochschalten =
		{
			.idm	= 0xFFFFFFFF,
			.id		= Schalten_Hoch_ID,
			.length	= 1
		};
		CAN_enableMOB(14, RECEIVE_DATA, MOb_Hochschalten);
		CAN_sendData(14, MOb_Hochschalten);
		CAN_disableMOB(14);

		timer_interrupt = 200;
	}
}
