/*
 * dashboard.h
 *
 *  Created on: 31.01.2016
 *      Author: Michael
 */

#ifndef DASHBOARD_H_
#define DASHBOARD_H_


#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>           			// Zugriff auf IO's des Controllers
#include <avr/interrupt.h>				// Zugriff auf Interrupts
#include <string.h>
#include <util/delay.h> 				// Funktionalit�t f�r Verz�gerungen einbinden
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <inttypes.h>					// Datentyp Integer einbinden
#include <stdbool.h>					// Datentyp BOOL einbinden

#include "i2cmaster.h"
#include "uart.h"
#include "utils.h"
#include "ADC.h"
#include "can_ID.h"
#include "display.h"
#include "LED_Treiber.h"
#include "ADC.h"
#include "can.h"
#include "globvars.h"

union	Drehzahlen			Drehzahl_vorne;
union	Drehzahlen			Drehzahl_hinten;
union	Drehzahlen			Drehzahl_Motor;
union	Pedale				Pedal;
union	Federwege			Federweg_vorne;
union	Federwege			Federweg_hinten;
union	Not_Aus				Not_Aus;
union	Umgebung			Umgebung;
//union	Reifentemperatur	Reifentemperatur_vorne;
//union	Reifentemperatur	Reifentemperatur_hinten;
union	Sensoren_Antrieb	Sensor;
union	Temperaturen		Temperatur;
union	Fahrstufe			Fahrstufe;


#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#define sbi(ADDRESS,BIT) 	((ADDRESS) |= (1<<(BIT)))	// set Bit
#define cbi(ADDRESS,BIT) 	((ADDRESS) &= ~(1<<(BIT)))	// clear Bit
#define	toggle(ADDRESS,BIT)	((ADDRESS) ^= (1<<BIT))		// Bit umschalten

#define	bis(ADDRESS,BIT)	(ADDRESS & (1<<BIT))		// bit is set?
#define	bic(ADDRESS,BIT)	(!(ADDRESS & (1<<BIT)))		// bit is clear?

#ifndef	TRUE
	#define	TRUE	(1==1)
#elif !TRUE
	#error	fehlerhafte Definition fuer TRUE
#endif

#ifndef FALSE
	#define	FALSE	(1!=1)
#elif FALSE
	#error	fehlerhafte Definition fuer FALSE
#endif


#define ROUND(x)    ((unsigned) ((x) + .5))				// runde eine positive Zahl

#define LENGTH(x)	sizeof(x) / sizeof(int)				// Gibt die Anzahl an Elementen (Integer Werte) in einem Array zur�ck

// Light Sensor Control Registers
#define ALS_Addr						0b00100000
#define ALS_Configuration_Register		0x00
#define ALS_HT_Windows_Setting			0x01
#define ALS_LT_Windows_Setting			0x02
#define ALS_HR_Output_Data				0x04
#define ALS_WC_Output_Data				0x05
#define ALS_Interrup_Status				0x06

// Macros for push-button signal evaluation
#define Kl50_is_low			(PING & (1<<PING0))
#define Inv_Res_is_low		!(PING & (1<<PING1))
#define VDCU_Res_is_low		!(PING & (1<<PING2))
#define PedCal_is_low		!(PINE & (1<<PINE2))
#define SuspCal_is_low		!(PINE & (1<<PINE6))
#define Reserve_is_low		!(PINE & (1<<PINE7))

#define DRS_is_low			!(PINF & (1<<PINF7))
//#define SoC_Mode_is_low		!(PINF & (1<<PINF6))
//#define Para_OK_is_low		!(PINF & (1<<PINF5))
#define Reduce_SC_is_low	!(PIND & (1<<PIND2))
#define Raise_SC_is_low		!(PIND & (1<<PIND3))
#define Reduce_TV_is_low	!(PIND & (1<<PIND4))
#define Raise_TV_is_low		!(PINA & (1<<PINA5))
#define SoC_Mode_is_low		!(PINA & (1<<PINA6))
#define Para_OK_is_low		!(PINA & (1<<PINA7))

// Macros for shutdown-systems signal evaluation
#define IMD_is_low			!(PINF & (1<<PINF4))
#define BMS_is_low			!(PINF & (1<<PINF3))
#define BSPD_is_low			!(PINF & (1<<PINF2))
#define IS_is_low			!(PINF & (1<<PINF1))
#define SBD_is_low			!(PINF & (1<<PINF0))


void updateVehicleStatus (void);
void RT10_SoC (void);
void translation_CAN_Messages(void);
void Init_Variables(void);
void Translate_Parameter_VDCU(void);
void Init_Light_Sensor(void);
void getLightValue(void);
//uint8_t getMenuDisplay(Parameter * SwitchNr);

#endif /* DASHBOARD_H_ */
