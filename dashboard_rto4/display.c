/*
 * display.c
 *
 *  Created on: 12.02.2016
 *      Author: Michael Lipp
 */


#include "dashboard.h"


void writeDataDisplay(char data)
{
	CheckBusy();

	DDRC = 0xFF;					// Data-bits for Display are set as outputs
	sbi(PORTA,2);					// set RS-bit
	cbi(PORTA,1);					// set write condition

	PORTC = data;					// transmit data

	sbi(PORTA,0);					// Enable device
	wait(1);						// wait for execution command in device
	cbi(PORTA,0);					// Disable device
}

void writeInsDisplay(char ins)
{
	CheckBusy();
	DDRC = 0xFF;					// Data-bits for Display are set as outputs

	cbi(PORTA,2);					// clear RS-bit

	cbi(PORTA,1);					// set write condition

	PORTC = ins;					// transmit data

	sbi(PORTA,0);					// Enable device
	wait(1);						// wait for execution command in device
	cbi(PORTA,0);					// Disable device
}

unsigned char CheckBusy(void)
{
	unsigned char readData = 1;

	DDRC = 0x00;					// Data-bits for Display are set as input
	cbi(PORTA,2);					// set RS-bit
	sbi(PORTA,1);					// set read condition

	do
	{
		sbi(PORTA,0);				// Enable device
		wait(1);					// wait for execution command in device
		cbi(PORTA,0);				// Disable device
		readData = PINC;			// Get information from display-data-bits
		wait(1);
	}while(readData&0x80);			// check for busy flag

	DDRC = 0xFF;					// Data-bits for Display are set as outputs
	return readData;
}

void wait (unsigned char waitstates)
{
	unsigned char i=0;
	for (i=0; i<waitstates; i++)
	{
		asm("nop");				// nop: 'no operation'
	}
}

void initDisplay(void)
{
	cbi(PORTA,0);					// Disable device
	PORTC = 0x00;					// reset all data-ports
	sbi(PORTA,4);					// disable display-reset

	_delay_ms(500);

	writeInsDisplay(0b00110100);	// 8-bit, RE=1
	writeInsDisplay(0b00001001);	// 4-line mode
	writeInsDisplay(0b00110000);	// 8-bit, RE=0
	writeInsDisplay(0b00110000);	// 8-bit, RE=0
	writeInsDisplay(0b00110000);	// 8-bit, RE=0
	writeInsDisplay(0b00000010);	// Cursor at home
	writeInsDisplay(0b00001100);	// Display on, cursor on, cursor blink
	writeInsDisplay(0b00000001);	// clear Display
	writeInsDisplay(0b00000110);	// return home

	unsigned char Grad[8] = {0x1C, 0x14, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen

	defineCharacter(Nr_Grad, Grad);																		// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('2')
}

void writeCharDisplay (char character)
{
	writeDataDisplay(character);
}

void writeStringDisplay (const char * string)
{
	do
	{
		writeDataDisplay(*string++);
	}while(*string);
}

void setSymbolDisplay (char add, char data)
{
	unsigned char i=0;
	unsigned char cursorpos = GETCURSORADDR();
	writeInsDisplay(0x36);
	writeInsDisplay(add);
	if(data&0x80)
	{
		for (i=0; i<16; i++)
			writeDataDisplay(0x00);
	}
	else if(data&0x20)
	{
		data&=0x5F;
		for (i=0; i<16; i++)
			writeDataDisplay(data);
	}
	else
		writeDataDisplay(data);
	writeInsDisplay(0x30);
	setPositionDisplay(cursorpos);
}

void setPositionDisplay (char pos)
{
	writeInsDisplay(LCD_HOME_L1+pos);
}

void DisplayOnOff (char data)
{
	writeInsDisplay(0x08 + data);
}

void defineCharacter (unsigned char position, unsigned char *data)
{
	unsigned char i=0;
	writeInsDisplay(0x40+8*position);

	for (i=0; i<8; i++)
	{
		writeDataDisplay(data[i]);
	}
//	setPositionDisplay(LINE1);
}

void clearDisplay (void)
{
	writeInsDisplay(0x01);
	setPositionDisplay(LINE1);
}

void DisplayMenuMesswerte (void)
{
	//Zeile 1 im Display mit aktuellem Wert f�r Motordrehzahl aus der Variable f�llen
	setPositionDisplay(LINE1+6);
	writeStringDisplay("             ");
	setPositionDisplay(LINE1+6);
	uint8_t current_gear;
	switch(Fahrstufe.translation.value){
		case 6: current_gear = 0;
			break;
		case 0: current_gear = 2;
			break;
		case 1: current_gear = 1;
			break;
		case 2: current_gear = 2;
			break;
		case 3: current_gear = 3;
			break;
		case 4: current_gear = 4;
			break;
		default: current_gear = 6;
			break;
		
	}
	sendDisplayDigit(current_gear);

	//Zeile 2 im Display mit aktuellem Wert f�r Motordrehzahl aus der Variable f�llen
	setPositionDisplay(LINE2+6);
	writeStringDisplay("           ");
	setPositionDisplay(LINE2+6);
	sendDisplayDigit(Drehzahl_Motor.translation.links);
	writeStringDisplay(" RPM");

	//Zeile 3 im Display mit aktueller Motortemperatur 1 und 2 aus den Variablen f�llen

	uint8_t Motor_Temp 			= Temperatur.translation.Motor_1 / 10;
	uint8_t Motor_Temp_milli 	= Temperatur.translation.Motor_1 - (Motor_Temp * 10);

	setPositionDisplay(LINE3+6);
	writeStringDisplay("             ");
	setPositionDisplay(LINE3+6);
	sendDisplayDigit(Motor_Temp);
	writeStringDisplay(",");
	sendDisplayDigit(Motor_Temp_milli);
	writeCharDisplay(Nr_Grad);
	writeStringDisplay("C  ");

	//Zeile 4 im Display mit aktuellem Wert der Batteriespannung aus der Variable f�llen

	uint8_t Bat_volt 		= Umgebung.translation.Batterie / 10;
	uint8_t Bat_millivolt 	= Umgebung.translation.Batterie - (Bat_volt * 10);

	setPositionDisplay(LINE4+6);
	writeStringDisplay("             ");
	setPositionDisplay(LINE4+6);
	sendDisplayDigit(Bat_volt);
	writeStringDisplay(",");
	sendDisplayDigit(Bat_millivolt);
	writeStringDisplay("V");
}

void sendDisplayDigit(uint32_t value)
{
/*
 * Runterbrechen eines vorzeichenunbehafteten Integer-Wertes einer beliebigen Variable mit maximal 32bit auf die einzelnen Ziffern, da diese
 * separat an das Display �bertragen werden m�ssen.
 * Anschlie�endes �bertragen der Ziffern an das Display.
*/
	uint8_t value_unit 				= 0;
	uint8_t value_ten 				= 0;
	uint8_t value_hundred 			= 0;
	uint8_t value_thousand 			= 0;
	uint8_t value_tenthousand 		= 0;
	uint8_t value_hundredthousand 	= 0;
	uint8_t value_million 			= 0;
	uint8_t value_tenmillion		= 0;

	if(value >= 10000000)
	{
		value_tenmillion = value / 10000000;
		value -= value_tenmillion * 10000000;
		writeCharDisplay(48+value_tenmillion);

		if(value < 1000000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 1000000)
	{
		value_million = value / 1000000;
		value -= value_million * 1000000;
		writeCharDisplay(48+value_million);

		if(value < 100000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 100000)
	{
		value_hundredthousand = value / 100000;
		value -= value_hundredthousand * 100000;
		writeCharDisplay(48+value_hundredthousand);

		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 10000)
	{
		value_tenthousand = value / 10000;
		value -= value_tenthousand * 10000;
		writeCharDisplay(48+value_tenthousand);

		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 1000)
	{
		value_thousand = value / 1000;
		value -= value_thousand * 1000;
		writeCharDisplay(48+value_thousand);

		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 100)
	{
		value_hundred = value / 100;
		value -= value_hundred * 100;
		writeCharDisplay(48+value_hundred);

		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 10)
		value_ten = value / 10,
		value -= value_ten * 10,
		writeCharDisplay(48+value_ten);

	if(value >= 0)
		value_unit = value,
		writeCharDisplay(48+value_unit);
}
