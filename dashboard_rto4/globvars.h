/*
 * globvars.h
 *
 *  Created on: 01.06.2017
 *      Author: Michael
 */

#ifndef GLOBVARS_H_
#define GLOBVARS_H_


union Drehzahlen
{
	struct byteorder_RPM
	{
		uint16_t links				:16;
		uint16_t rechts				:16;
	}translation;
	uint8_t data [4];
};

union Pedale
{
	struct byteorder_pedals
	{
		uint16_t Bremse				:16;
		uint16_t Kupplung			:16;
		uint16_t Leerlauf			:16;
		uint16_t Drossel			:16;
	}translation;
	uint8_t data [8];
};

union Federwege
{
	struct byteorder_suspension
	{
		uint16_t links				:16;
		uint16_t rechts				:16;
	}translation;
	uint8_t data [4];
};

union Not_Aus
{
	struct byteorder_emergency
	{
		uint16_t vorn				:16;
		uint16_t Dashboard			:16;
	}translation;
	uint8_t data [4];
};

union Umgebung
{
	struct byteorder_ambient
	{
		uint16_t Batterie			:16;
		uint16_t Temp_Umgebung		:16;
		uint16_t Laptime			:16;
	}translation;
	uint8_t data [6];
};

union Reifentemperaturen
{
	struct byteorder_wheeltemp
	{
		uint16_t links				:16;
		uint16_t mitte				:16;
		uint16_t rechts				:16;
	}translation;
	uint8_t data [6];
};

union Sensoren_Antrieb
{
	struct byteorder_sensors
	{
		uint16_t Lambda				:16;
		uint16_t Benzin_p			:16;
		uint16_t Oel_p				:16;
		uint16_t Tank				:16;
	}translation;
	uint8_t data [8];
};

union Temperaturen
{
	struct byteorder_temperatures
	{
		uint16_t Oel				:16;
		uint16_t Motor_1			:16;
		uint16_t Motor_2			:16;
	}translation;
	uint8_t data [6];
};

union Fahrstufe
{
	struct byteorder_gear
	{
		uint8_t value				:8;
	}translation;
	uint8_t data [1];
};

extern union	Drehzahlen			Drehzahl_vorne;
extern union	Drehzahlen			Drehzahl_hinten;
extern union	Drehzahlen			Drehzahl_Motor;
extern union	Pedale				Pedal;
extern union	Federwege			Federweg_vorne;
extern union	Federwege			Federweg_hinten;
extern union	Not_Aus				Not_Aus;
extern union	Umgebung			Umgebung;
extern union	Reifentemperatur	Reifentemperatur_vorne;
extern union	Reifentemperatur	Reifentemperatur_hinten;
extern union	Sensoren_Antrieb	Sensor;
extern union	Temperaturen		Temperatur;
extern union	Fahrstufe			Fahrstufe;


enum DisplayCharacters
{
	Nr_Grad			= 0
};
#endif /* GLOBVARS_H_ */
