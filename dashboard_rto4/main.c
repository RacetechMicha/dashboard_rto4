/*
 * main.c
 *
 *  Created on: 21.01.2016
 *      Author: Michael
 */

/*#################################################################################################
	Fuer	: Dashboard RTo4
	Version	: 1.1
	Autor	: Michael Lipp
	License	: GNU General Public License
	Hardware: AT90CAN128, 16MHz,
	Aufgabe :
//##################################################################################################
	kein watchdog moeglich ... sonst controller bei softwarereset im Bootloadermodus
//################################################################################################*/


#include "dashboard.h"

volatile int16_t			timer_run_LED			= 899,
							timer_LED_Drehzahl		= 0,
							timer_update_Display 	= 250,
							timer_interrupt			= 200,
							timer_flash_LED_red		= 0,
							timer_send_CAN			= 500;

volatile uint8_t 			counter_CAN_status		= 0,
							counter_CAN_parameter	= 0;

void initialisierung(void)
{
		wdt_disable();													//watchdog disable
		cli();															//disable interrupts

//I�C

		i2c_init();														//initialisiere I�C-Bus

// Timer 0 and CTC-Mode for Pin B7 (Buzzer)
//		TCCR0A		|= (1<<WGM01) |(1<<COM0A0)| (1<<CS01)| (1<<CS00);	//Prescaler 256; CTC-Mode, Clear on compare match
//		OCR0A		= 10;												//halbe Pulsbreite

// Timer 1 and PWM-Mode for Pin B5 (DRS)
//		TCCR1A  |=  (1<<COM1A1)| (1<<WGM11);							// pin B5 und PWM mode	--> timer 1
//		TCCR1B 	|=   _BV(WGM13)| _BV(WGM12) |_BV(CS10)|_BV(CS11);		// timer1 prescaller 1023; _BV(CS10)|  _BV(CS12)|
//		ICR1 	=	2000;
//		OCR1A 	= 	300;

// Timer 2 for code-internal operations
		TCCR2A		= (1<<CS22);										// Prescaler 64
		TCNT2		= 5;												// 8-bit Timer, also 255 - 5 (TCNT = 5) Schritte -> 1000 Schritte pro Sekunde -> 1,0ms!
		TIMSK2		|= _BV(TOIE2);

//#######################################	Externe Interrupts

	EICRA	= 0b11100000;												// Register A INT 0- 3  // Lenkrad Knopf Schalten runter und Lenkrad Knopf Schalten hoch	// 0 und 0: low level request; 0 und 1: request for any level-change; 1 und 0: falling edge; 1 und 1: rising edge
//	EICRB	= 0b11111111;					 							// Register B INT 4- 7	//

	// Interrupts aktivieren
	EIMSK	= 0b00001100;												// INT2 + INT3 aktivieren

//	EIMSK 	|= 	_BV(INT7 )| 											//Reserve
//				_BV(INT6 )|												//Susp_Cal
//				_BV(INT5 )|												//VDCU_Res
//				_BV(INT4 )|												//Para_OK
//				_BV(INT3 )|												//Inv_Res
//				_BV(INT2 );												//Start

// CAN-Empfang mit Interrupt-gest�tzem Einlesen der Nachrichten

		CAN_init(500, RX);												// Bus mit Baudrate von 500 kBaud initialisieren und Receive-Interrupts aktivieren
		define_MObs();													// Message Objekte definieren und aktivieren


//####################################### enable interrupts
	_delay_ms(100);
		sei();								// enable interrupts

//Definition of Controller-Pins

	//Display
		DDRA |= (1 << PA0);			//LCD_E
		DDRA |= (1 << PA1);			//LCD_RW
		DDRA |= (1 << PA2);			//LCD_RS
		DDRA |= (1 << PA4);			//LCD_Reset

//		DDRC |= (1 << PC0);			//LCD_D0
//		DDRC |= (1 << PC1);			//LCD_D1
//		DDRC |= (1 << PC2);			//LCD_D2
//		DDRC |= (1 << PC3);			//LCD_D3
//		DDRC |= (1 << PC4);			//LCD_D4
//		DDRC |= (1 << PC5);			//LCD_D5
//		DDRC |= (1 << PC6);			//LCD_D6
//		DDRC |= (1 << PC7);			//LCD_D7

		DDRC = 0b11111111;			//Alle Pins als Ausg�nge f�r Port C

		DDRA &= ~(1 << PA5);		//AT90_in1
		DDRA &= ~(1 << PA6);		//AT90_in2
		DDRA &= ~(1 << PA7);		//AT90_in3
		DDRD &= ~(1 << PD2);		//AT90_in4
		DDRD &= ~(1 << PD3);		//AT90_in5
		DDRD &= ~(1 << PD4);		//AT90_in6

		//PullUps anschalten
		PORTA |= (1 << PA5);
		PORTA |= (1 << PA6);
		PORTA |= (1 << PA7);
		PORTD |= (1 << PD2);
		PORTD |= (1 << PD3);
		PORTD |= (1 << PD4);

}

ISR(SIG_OVERFLOW2)
{
	TCNT0  		= 	5;			 					// Timer 0, overflow nach 1 ms

	timer_run_LED--;		if (timer_run_LED			<= 0)	{timer_run_LED			= 0;}
	timer_update_Display--;	if (timer_update_Display	<= 0)	{timer_update_Display	= 0;}
	timer_interrupt--;		if (timer_interrupt			<= 0)	{timer_interrupt		= 0;}
	timer_LED_Drehzahl--;	if (timer_LED_Drehzahl		<= 0)	{timer_LED_Drehzahl		= 0;}
	timer_flash_LED_red--;	if(timer_flash_LED_red 		<= 0)	{timer_flash_LED_red	= 0;}
	timer_send_CAN--;		if (timer_send_CAN			<= 0)	{timer_send_CAN			= 0;}
}


int main(void)
{
	initialisierung();									// Controller-Pins, Bussysteme, Interrupts und Timer initialisieren
	_delay_ms(250);										// Auf die Ausf�hrung der Inits und das Hochfahren der VDCU warten
	initDisplay();										// Den Betriebsmodus des Displays festlegen und den Cursor auf 'Nullstellung' bringen
	initialize_LED_Treiber();							// Den Betriebsmodus des LED-Treibers festlegen und alle LEDs einschalten (LEDs k�nnten auch �ber PWM helligkeitsgesteuert werden)
	Init_Variables();

	switch_LED_all(LED_ON);
	timer_run_LED = 899;

	while(timer_run_LED > 0)
	{
		run_LED_red_2();
		_delay_ms(50);
	}

	switch_LED_all(LED_OFF);								// Alle LEDs zum Initialisieren erstmal ausschalten, werden sp�ter nach Bedarf zugeschaltet

	setPositionDisplay(LINE1);
	writeStringDisplay("GEAR:");
	setPositionDisplay(LINE2);
	writeStringDisplay("REVS:");
	setPositionDisplay(LINE3);
	writeStringDisplay("TEMP:");
	setPositionDisplay(LINE4);
	writeStringDisplay("BATT:");

	while(1)
	{
		if(timer_update_Display <= 0)
		{
			DisplayMenuMesswerte();
			timer_update_Display = 150;
		}
		if(timer_LED_Drehzahl <= 0)
		{
			DrehzahlMotor();
			timer_LED_Drehzahl = 10;
		}
		if(timer_send_CAN <= 0)
		{
			send_CAN_Status();
			timer_send_CAN = 500;
		}
	}
	return 0;
}


